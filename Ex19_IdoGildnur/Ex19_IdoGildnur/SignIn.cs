﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex19_IdoGildnur
{
    public partial class SignIn : Form
    {
        private String userName;
        private String passWord;

        public SignIn()
        {
            this.userName = "";
            this.passWord = "";
            InitializeComponent();
        }

        //The function saves the user's username.
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            this.userName = userName_input.Text;
        }

        //The function checks for validation of the username and password and for continuing
        // to next Form (the calender).
        private void button1_Click(object sender, EventArgs e)
        {
            if (!CheckForValidation(this.userName, this.passWord)) //Sends data for validation-checking
                MessageBox.Show("Wrong username or wrong password, Try again");
            else
            { //Continuing to the next Form (Calender) and closing this Form.
                Calender c = new Calender(this.userName);
                this.Hide();
                c.ShowDialog();
                this.Close();
            }
            
        }

        //The function save the user's password.
        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            this.passWord = passWord_input.Text;
        }

        //The function validates the user's username and password.
        public bool CheckForValidation(string username, string password)
        {
            string[] fixUser = new string[2];
            foreach (string line in File.ReadLines("C:\\Users\\magshimim\\Documents\\Magshimim\\11TH_GRADE\\semester_b\\advanced_prin\\EX19\\Users.txt"))
            {
                if (line.Contains(username) && line.Contains(password) && line.StartsWith(username))
                {
                    fixUser = line.Split(','); //parting the line to username and password into the array.
                    if (fixUser[0] == username && fixUser[1] == password)
                        return true;
                }
            }
            return false;
        }

        //The function finishes the programm when 'canceled'.
        private void canceButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
