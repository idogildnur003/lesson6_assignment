﻿namespace Ex19_IdoGildnur
{
    partial class SignIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userName_input = new System.Windows.Forms.TextBox();
            this.passWord_input = new System.Windows.Forms.TextBox();
            this.Login_button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.canceButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // userName_input
            // 
            this.userName_input.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userName_input.Location = new System.Drawing.Point(158, 33);
            this.userName_input.Name = "userName_input";
            this.userName_input.Size = new System.Drawing.Size(133, 30);
            this.userName_input.TabIndex = 0;
            this.userName_input.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // passWord_input
            // 
            this.passWord_input.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passWord_input.Location = new System.Drawing.Point(158, 83);
            this.passWord_input.Name = "passWord_input";
            this.passWord_input.PasswordChar = '*';
            this.passWord_input.Size = new System.Drawing.Size(133, 30);
            this.passWord_input.TabIndex = 1;
            this.passWord_input.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // Login_button
            // 
            this.Login_button.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Login_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Login_button.Location = new System.Drawing.Point(40, 147);
            this.Login_button.Name = "Login_button";
            this.Login_button.Size = new System.Drawing.Size(100, 35);
            this.Login_button.TabIndex = 2;
            this.Login_button.Text = "Login";
            this.Login_button.UseVisualStyleBackColor = false;
            this.Login_button.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(32, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Username:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(36, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Password:";
            // 
            // canceButton
            // 
            this.canceButton.BackColor = System.Drawing.SystemColors.ControlDark;
            this.canceButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.canceButton.Location = new System.Drawing.Point(171, 147);
            this.canceButton.Name = "canceButton";
            this.canceButton.Size = new System.Drawing.Size(100, 35);
            this.canceButton.TabIndex = 5;
            this.canceButton.Text = "Cancel";
            this.canceButton.UseVisualStyleBackColor = false;
            this.canceButton.Click += new System.EventHandler(this.canceButton_Click);
            // 
            // SignIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 194);
            this.Controls.Add(this.canceButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Login_button);
            this.Controls.Add(this.passWord_input);
            this.Controls.Add(this.userName_input);
            this.Name = "SignIn";
            this.Text = "Log In Screen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox userName_input;
        private System.Windows.Forms.TextBox passWord_input;
        private System.Windows.Forms.Button Login_button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button canceButton;
    }
}

