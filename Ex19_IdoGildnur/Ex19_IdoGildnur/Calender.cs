﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex19_IdoGildnur
{
    public partial class Calender : Form
    {
        private string username;

        public Calender(string username)
        {
            this.username = username;
            FileValidation(); //First checking for existance of user's file.
            InitializeComponent();
        }

        //Function checks for existance of file in directory.
        private void FileValidation()
        {
            try
            { // Creates new file if doesn't exists.
                if (!File.Exists(GetPath()))
                {
                    FileStream file = File.Create(GetPath());
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //Function returns the matched path for this user's name.
        public string GetPath()
        {
            return "C:\\Users\\magshimim\\Documents\\Magshimim\\11TH_GRADE\\semester_b\\advanced_prin\\EX19\\" + this.username + "DB.txt";
        }

        //The function displays the selected date's status - for birthdays.
        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            bool hasBirthday = false;
            string[] fileDateSelected = new string[2];
            DateTime selectedDate = monthCalendar1.SelectionRange.Start;
            string selected = selectedDate.Date.ToShortDateString();
            foreach (string line in File.ReadLines(GetPath()))
            {
                if (line.Contains(selected))
                {
                    fileDateSelected = line.Split(',');
                    if (fileDateSelected[1] == selected)
                        hasBirthday = true;
                }
            }
            if(hasBirthday)
                info_date_selected.Text = fileDateSelected[1] + " - " + fileDateSelected[0] + " Celebrates birthday";
            else
                info_date_selected.Text = "No Birthday for you...";
        }

        //The function adds entered name and date to the birthday's list of this user.
        private void AddNewName_Click(object sender, EventArgs e)
        {
            string newBirthBoy = adding_name.Text;
            string datePicker = dateTimePicker1.Value.ToShortDateString();
            File.AppendAllText(GetPath(), newBirthBoy + "," + datePicker + Environment.NewLine);
        }
    }
}
